# Sin

Sin is a stripped version of [gin-gonic/gin](https://github.com/gin-gonic/gin) that aims to keep things simple. If Gin
identifies itself as a "web framework", then Sin is a "web micro-service interface development library".

Sin does not hold your hand with helper functions such as "`ClientIP()`", which because of their high level of
abstraction from the simple principles and good "hygiene practices" of "web" software development leads to so called
"[security issues](https://github.com/gin-gonic/gin/pull/2474)" and then patches that
[break](https://github.com/gin-gonic/gin/issues/2697) peoples work.

Sin is for battle tested services and for developers that know what they're doing.

Until `v1` is released, more parts of the original Gin codebase will be stripped or modified to suit the development
needs of SUDOLESS, which aims to [KISS](https://en.wikipedia.org/wiki/KISS_principle).

All code to Sin is released under the [MPL 2.0 LICENSE](https://mozilla.org/MPL/2.0/).

## Why?

### Fewer dependencies

```shell
# gin
github.com/gin-contrib/sse v0.1.0
github.com/go-playground/validator/v10 v10.4.1
github.com/golang/protobuf v1.3.3
github.com/json-iterator/go v1.1.9
github.com/mattn/go-isatty v0.0.12
github.com/stretchr/testify v1.4.0
github.com/ugorji/go/codec v1.1.7
gopkg.in/yaml.v2 v2.2.8
```

```shell
# sin
github.com/stretchr/testify v1.7.0
```

Why pull all those dependencies and the dependencies dependencies, when I am not even using protobuf.

### Highly opinionated

Stripped out features that are of no use to us; those include, but are not limited to:

* Default JSON encoding/decoding (no third-parties, no JSONP, no SecureJSON, etc)
* No Bind* methods
* No assistive

If the stripped down version suits your needs, you're in luck. If not, this library is not for you.

### Less Abstraction

With bad abstraction comes complexity and confusion.

### For Professionals

You wouldn't drive a car without a license. You wouldn't perform surgery without a PhD. You wouldn't operate dangerous
machinery without the proper training. So why would you deploy software out into the world without knowing what you're
doing?!

Some machines cannot be made "safer" for the user without losing performance, or hiding features behind complexity. As
such we aim to make Sin a powerful tool, full of risks for those who do not know anything about their environment and
for those who do not read the manual.

## Mission

Sin `v1` will be released once as much of Gin has been replaced, and the projects gets closer to the simplicity of the
[httprouter](https://github.com/julienschmidt/httprouter) project, which Gin is build on.
